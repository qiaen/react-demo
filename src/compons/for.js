import React from 'react'

class CmtList extends React.Component {
	constructor(props) {
		super(props)
		this.state = { ...this.props }
	}
	render() {
		return (
			<section>
				<h1>{this.state.title}</h1> 
				<ul> 
					{this.state.data.map((item, index) => {
						return <li key={index}>{item.name} 说 {item.content}</li>
					})}
				</ul>
			</section>
		)
	}
}
export default CmtList