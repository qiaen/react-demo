import React from 'react'
import Layout from './layout.js'
class Tap extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			...props,
			child: {
				name: 'isChild',
				age: 1001,
				favorites: ['eat', 'sleep']
			}
		}
	}
	render() {
		return (
			<div>
				<Layout>
					<h3 name="header">Header, This is H3!</h3>
					<h4 name="main">Main, This is H4!</h4>
				</Layout>
				<div>Hello, {this.state.name}</div>
				<button onClick={this.bindtap.bind(this)}>click</button>
				<input value={this.state.name} onChange={this.bindchange.bind(this)}></input>
				<h1>CHILD</h1>
				<h2>{this.state.child.name}, age is {this.state.child.age}</h2>
				<ul>
					{
						this.state.child.favorites.map(item => {
							return <li key={item}>{item}</li>
						})
					}
				</ul>
			</div>
		)
	}
	bindtap() {
		let child = this.state.child
		child.age++
		child.favorites.push(child.age)
		this.setState({
			name: 'Cissy',
			child
		})
	}
	bindchange(e) {
		this.setState({
			name: e.target.value
		})
	}
	componentDidMount() {
		/*this.setState({
			name: 'Jessic'
		})*/
	}
}
export default Tap