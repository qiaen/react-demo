import React from 'react'
class Layout extends React.Component {
	constructor(props) {
		super(props)
		this.state = {

		}
		this.renderChild = this.renderChild.bind(this)
	}
	render() {
		return (
			<div>
				<header className="header">
					{this.props.children.map(solt => {
						return this.renderChild(solt, 'header')
					})}
				</header>
				<main className="main">
					{this.props.children.map(solt => {
						return this.renderChild(solt, 'main')
					})}
				</main>
				<footer className="footer">
					{this.props.children.map(solt => {
						return this.renderChild(solt, 'footer')
					})}
				</footer>
			</div>
		)
	}
	renderChild(child, type) {
		if (child.props.name === type) {
			// child.props.newname = parseInt(Math.random()*100)
			// console.log(child.props)
			return (<div key={child.props.name}>{child}</div>)
		}
	}
}
export default Layout