import React from 'react'
import Orders from '../complaint/complaint.js'

class Complaint extends React.Component {
	constructor(props) {
		super(props)
		// console.log(props)
		this.state = {
			tabs: []
		}
	}
	componentDidMount() {
		setTimeout(() => {
			this.setState({
				tabs: [
					{ tab: '腾讯' },
					{ tab: '360' },
					{ tab: 'baidu' },
					{ tab: 'Ali' },
				]
			})
		}, 1000)
	}
	render() {
		return (
			<div>
				<Orders tabs={this.state.tabs}></Orders>
			</div>
		)
	}
}
export default Complaint