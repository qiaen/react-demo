import React from 'react'
import { connect } from 'react-redux'

import { Table, Button } from 'antd'

const mapStateToProps = state => {
	console.log('------mapStateToProps-----', this)
	console.log(state)
	return {}
};
class Order extends React.Component {
	constructor(props) {
		super(props)
		console.log('---------7')
		console.log(props)
		this.state = {
			dataSource: [{
				key: '1',
				name: '胡彦斌',
				age: 32,
				address: '西湖区湖底公园1号',
			}],
			// 是否显示编辑弹窗
			visible: true,
			columns: [{
				title: '姓名',
				dataIndex: 'name',
				key: 'name',
			},
			{
				title: '年龄',
				dataIndex: 'age',
				key: 'age',
			},
			{
				title: '住址',
				dataIndex: 'address',
				key: 'address',
			},
			{
				title: '操作',
				dataIndex: 'set',
				key: 'set',
				render: (text, row) => {
					return <Button type="primary" onClick={() => this.showAdd(row)}>edit</Button>
				}
			}]
		}
	}
	render() {
		return (
			<>
				<Table size="small" dataSource={this.state.dataSource} scroll={{ y: 300 }} columns={this.state.columns} />
			</>
		)
	}
	showAdd(row) {
		this.props.showAdd(row)
	}
	// 用户搜索
	toSearch(val) {
		console.log('child toSearch', val)
	}
	// 添加一条新数据
	addList(row) {
		console.log(123, row)
		// 编辑
		console.log(row.key)
		if(row.key) {
			let dataSource = this.state.dataSource.map(item => {
				if(row.key===item.key){
					return row
				} else {
					return item
				}
			})
			this.setState({
				dataSource
			})
		} else {
			this.setState({
				dataSource:[...this.state.dataSource, { ...row, key: new Date().getTime() }]
			})
		}
		
		console.log(this.state.columns)
	}
	componentDidMount() {
		// 挂载子组件到父组件
		this.props.setRef(this)
	}
	componentDidUpdate() {
		console.log('componentDidUpdate')
		console.log('tables', this.props)
	}
	
}
export default connect(mapStateToProps)(Order)