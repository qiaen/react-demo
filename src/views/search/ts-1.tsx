import React from 'react'
import { Form } from 'antd'
let name: string ='type script'
// 装饰器
function Animals(p: any) {
    console.log(p, name)
    p.prototype.name = 'hello'
}
// @Animals
// class Dog {
//     constructor() {
//         console.log('Dog')
//     }
// }

// let hashiqi:any = new Dog()
// console.log(hashiqi.name)

// interface 接口 https://ts.xcatliu.com/basics/type-of-object-interfaces.html
interface Cat {
    // 只有在初始化定义时被赋值，单独修改不被允许
    readonly id: number,
    name: string,
    age: number|string,
    details: any,
    // 用来表示属性可无
    favorites?: any,
    // 用来新增属性
    [propName:string]:any
}

// 这里可以赋值id
let p1: Cat = {
    id: 123,
    name:'lili',
    age:1,
    details: 'hello, lili cat',
    selected: true
}
//会报错，因为id是只读属性
// p1.id = 134 // Cannot assign to 'id' because it is a read-only property
console.log(p1)

// 函数 function
function f1(p: number): string {
    return p.toString()
}
console.log(f1(233))

// void 空值,表示undefined或者bull
// 也可以用null定义
let b1: null|undefined = null
//  也可以表示没有任何返回，但是一般不这样写
function f2(): void {
    console.log(1)
}
// undefined和null是任何类型的子集,比如下面也不会报错
let a1:number = 1
let a2:number  //都不会报错

// 类型断言
// 当 TypeScript 不确定一个联合类型的变量到底是哪个类型的时候，我们只能访问此联合类型的所有类型中共有的属性或方法：
// 而有时候，我们确实需要在还不确定类型的时候就访问其中一个类型特有的属性或方法，比如
interface Cat1 {
    name: string
}
interface Fish1 {
    name: string,
    swim(): void
}
// 这样就可以解决访问 animal.swim 时报错的问题了。

// 需要注意的是，类型断言只能够「欺骗」TypeScript 编译器，无法避免运行时的错误，反而滥用类型断言可能会导致运行时错误：
function isFish1(animal: Cat1 | Fish1) {
    if (typeof (animal as Fish1).swim === 'function') {
        return true;
    }
    return false;
}
class Person {
    iName: string = '山顶洞人'
    static sName: string = '世纪末'
    constructor(val: any) {
        console.log('----', val)
        this.iName = val||'原始人'
    }
    static backiName() {
        // 获取this.iName会报错，因为还没有实例化
        // console.log('this.iName----', this.iName)
        return 'hello backIname'
    }
    sayiName() {
        return this.iName
    }
}
console.log('--------------------------')
// let per1 = new Person('hello , san')
// console.log('sayiName---',per1.sayiName())
console.log('backiName----', Person.backiName())
console.log(Person.sName)
class Dom extends React.Component {
	constructor(props: any) {
		super(props)
		this.state = {
			keywords:''
		}
	}
	render() {
        // return <span>hello,{name}</span>
        return null
    }
}
export default Form.create()(Dom)