import React from 'react'
import { connect } from "react-redux"
import { addTodo } from "../../store/actions"
import { Input, Button } from 'antd'
// import './index.scss';
const { Search } = Input

// console.log(addTodo)
class Dom extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			value: ''
		}

	}
	add(val) {
		// this.props.addTodo(val)
		this.setState({
			value: ''
		})
		// 把input的搜索信息传递给父组件
		this.props.searchHub(val)
	}
	render() {
		return (
			<div className="search flex">
				<Search enterButton="Search" onChange={e => this.setState({value: e.target.value})} value={this.state.value} onSearch={value => this.add(value)} placeholder="input search text" className="mb15 search-box" />
				<div className="flex1 txright">
					{/* 实现一个插槽 */}
					{this.props.children.find(c => c.key==='Clear')}
					<Button onClick={this.props.showAdd.bind()} type="primary">Add</Button>
					{this.props.children.find(c => c.key==='Select')}
				</div>
			</div>
		)
	}
}
export default connect(null, { addTodo })(Dom)