import React from 'react'
import Search from './search.jsx'
import Tables from './tables.jsx'
import Edit from './edit.jsx'
import { Button } from 'antd'
class Dom extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			keywords: '',
			visible: false,
			data: {}
		}
	}
	custSetState = (obj) => {
		console.log('obj------', obj)
		// this.setState(obj)
	}
	render() {
		return (
			<div>
				<Search {...this.searchProps}>
					<Button key="Clear">Clear</Button>
					<Button key="Select">Select</Button>
				</Search>
				<Tables {...this.tablesProps} className="mt10"></Tables>
				<Edit {...this.editProps}></Edit>
			</div>
		)
	}
	// 搜索
	get searchProps() {
		let that = this
		return {
			showAdd: val=> {
				that.setState({visible: true})
			},
			searchHub: keywords => {
				this.setState({
					keywords
				})
				console.log('searchHub keywords', keywords)
				this.childTable.toSearch(keywords)
			}
		}
	}
	// 表单
	get tablesProps() {
		return {
			// 子组件用showAdd(row)，this指向当前
			// 子组件用showAdd = () => this指向子组件
			showAdd: val => {
				this.setState({visible: true})
				// this.childEdit.mapData(val)
				this.setState({
					data: val
				})
			},
			setRef: obj => {
				this.childTable = obj
			},
			keywords: this.state.keywords
		}
	}
	// 编辑
	get editProps() {
		let that = this
		return {
			closeEdit(row){
				that.setState({
					visible: false,
					data: null
				})
				if(row) {
					console.log('closeEdit', that.childTable)
					that.childTable.addList(row)
				}
			},
			setRef: obj => {
				this.childEdit = obj
			},
			data: this.state.data,
			visible: this.state.visible
		}
	}
}
export default Dom