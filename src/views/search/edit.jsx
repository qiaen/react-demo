import React from 'react'
import { Drawer, Button, Form, Input, Select } from 'antd'
const { Option } = Select
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 }
}
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
}
class Dom extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			keywords:''
		}
	}
	render() {
        const form = this.props.form
        const data = this.props.data || {
            name: '',
            age: 10,
            address: ''
        }
		const { getFieldDecorator } = form
		return (
			<Drawer title={data.key ? `编辑 - ${data.name}` : '新增'} placement="right" closable={false} onClose={this.onClose} visible={this.props.visible}  width={500}>
                <Form {...layout} name="control-ref">
                    <Form.Item name="name" label="姓名" rules={[{ required: true }]}>
                        {getFieldDecorator('name', { initialValue: data.name,rules: [{ required: true, message: '必填' }] })(
                            <Input placeholder="姓名"/>
                        )}
                    </Form.Item>
                    <Form.Item name="age" label="年龄" rules={[{ required: true }]}>
                        {getFieldDecorator('age', { initialValue: data.age,rules: [{ required: true, message: '必填' }] })(
                            <Select placeholder="选择年龄" allowClear>
                                <Option value="10">10</Option>
                                <Option value="20">20</Option>
                                <Option value="30">30</Option>
                            </Select>
                        )}
                    </Form.Item>
                    <Form.Item name="address" label="住址" rules={[{ required: true }]}>
                        {getFieldDecorator('address', { initialValue: data.address,rules: [{ required: true, message: '必填' }] })(
                            <Input placeholder="地址"/>
                        )}
                    </Form.Item>
                    <Form.Item {...tailLayout}>
                        <Button onClick={this.submit} type="primary" htmlType="submit">Submit</Button>
                    </Form.Item>
                </Form>
            </Drawer>
		)
    }
    // 把信息放入编辑表单
    mapData(form) {
        // this.props.form.setFieldsValue({
		// 	field: undefined
		// })
    }
    // 关闭edit
    onClose = ()=> {
        this.props.closeEdit()
    }
    // 保存
    submit = values => {
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                console.log('valuessssss', values)
                this.props.closeEdit({...values, key: (this.props.data || {}).key||null})
            }
        })
    }
	get searchProps() {
		return {
			searchHub: keywords => {
				this.setState({
					keywords
				})
				console.log('searchHub keywords', keywords)
				this.childTable.toSearch(keywords)
			}
		}
	}
	get tablesProps() {
		return {
			setRef: obj => {
				this.childTable = obj
			},
			keywords: this.state.keywords
		}
    }
    componentDidMount() {
		// 挂载子组件到父组件
		this.props.setRef(this)
	}
}
export default Form.create()(Dom)