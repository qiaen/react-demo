import React from 'react'
import './order.css';
import { Table } from 'antd'
import data from './data.js'
let { result } = data
let columns = [{
		"title": "老师ID",
		"dataIndex": "teaId",
		"key": "teaId",
		"description": null,
		"isRender": null,
		"fixed": "left",
		"sorter": (a, b) => a.teaId - b.teaId,
		"filters": [
			{ text: 'Male', value: 'male' },
			{ text: 'Female', value: 'female' },
		],
		"ellipsis": true
	},
	{
		"title": "老师姓名",
		"dataIndex": "teaName",
		"key": "teaName",
		"description": null,
		"isRender": null,
		"ellipsis": true
	},
	{
		"title": "老师类型",
		"dataIndex": "timeType",
		"key": "timeType",
		"description": null,
		"isRender": null,
		"sorter": (a, b) => a.teaId - b.teaId,
		"filters": [
			{ text: 'Male', value: 'male' },
			{ text: 'Female', value: 'female' },
		],
		"ellipsis": true
	},
	{
		"title": "老师状态",
		"dataIndex": "teaState",
		"key": "teaState",
		"description": null,
		"isRender": null,
		"ellipsis": true
	},
	{
		"title": "老师手机号",
		"dataIndex": "teaMobile",
		"key": "teaMobile",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程科目",
		"dataIndex": "lesSubject",
		"key": "lesSubject",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程类型",
		"dataIndex": "lesType",
		"key": "lesType",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程班型",
		"dataIndex": "lesMode",
		"key": "lesMode",
		"description": null,
		"isRender": null
	},
	{
		"title": "学生ID",
		"dataIndex": "stuId",
		"key": "stuId",
		"description": null,
		"isRender": null
	},
	{
		"title": "学生姓名",
		"dataIndex": "stuName",
		"key": "stuName",
		"description": null,
		"isRender": null
	},
	{
		"title": "学生年级",
		"dataIndex": "stuGrade",
		"key": "stuGrade",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程ID",
		"dataIndex": "lessonId",
		"key": "lessonId",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程开始时间",
		"dataIndex": "lesStartedAt",
		"key": "lesStartedAt",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程结束时间",
		"dataIndex": "lesEndedAt",
		"key": "lesEndedAt",
		"description": null,
		"isRender": null
	},
	{
		"title": "课程总结状态",
		"dataIndex": "lesResultDetail",
		"key": "lesResultDetail",
		"description": null,
		"isRender": null
	},
	{
		"title": "是否有语音点评",
		"dataIndex": "lesVoiceDuration",
		"key": "lesVoiceDuration",
		"description": null,
		"isRender": null
	},
	{
		"title": "文字点评内容",
		"dataIndex": "lesSummedUp",
		"key": "lesSummedUp",
		"description": null,
		// onCell: (val) => {
		// 	console.log(val)
		// }
		"ellipsis": true
		// "isRender": null,
		// render: (val) => <span className="table-span" title="{{val}}">{val}</span>
	},
	{
		"title": "是否有作业",
		"dataIndex": "lesIsHomework",
		"key": "lesIsHomework",
		"description": null,
		"isRender": null
	},
	{
		"title": "PC_PV",
		"dataIndex": "les_pc_pv",
		"key": "les_pc_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "PC_UV",
		"dataIndex": "les_pc_uv",
		"key": "les_pc_uv",
		"description": null,
		"isRender": null
	},
	{
		"title": "Pad_PV",
		"dataIndex": "les_pad_pv",
		"key": "les_pad_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "Pad_UV",
		"dataIndex": "les_pad_uv",
		"key": "les_pad_uv",
		"description": null,
		"isRender": null
	},
	{
		"title": "APP_PV",
		"dataIndex": "les_app_pv",
		"key": "les_app_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "APP_UV",
		"dataIndex": "les_app_uv",
		"key": "les_app_uv",
		"description": null,
		"isRender": null
	},
	{
		"title": "微信_PV",
		"dataIndex": "les_wechat_pv",
		"key": "les_wechat_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "微信_UV",
		"dataIndex": "les_wechat_uv",
		"key": "les_wechat_uv",
		"description": null,
		"isRender": null
	},
	{
		"title": "销售后台_PV",
		"dataIndex": "les_sel_pv",
		"key": "les_sel_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "销售后台_UV",
		"dataIndex": "les_sel_uv",
		"key": "les_sel_uv",
		"description": null,
		"isRender": null
	},
	{
		"title": "其他_PV",
		"dataIndex": "les_other_pv",
		"key": "les_other_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "其他_UV",
		"dataIndex": "les_other_uv",
		"key": "les_other_uv",
		"description": null,
		"isRender": null
	},
	{
		"title": "总PV",
		"dataIndex": "total_pv",
		"key": "total_pv",
		"description": null,
		"isRender": null
	},
	{
		"title": "总UV",
		"dataIndex": "total_uv",
		"key": "total_uv",
		"description": null,
		"isRender": null
	}
]
class Order extends React.Component {
	componentDidMount() {
		this.setState({
			data: result
		})
		window.onresize = () => {
			// 页面缩放，只负责重新设置表格高度
			this.setTableHeight()
		}
	}
	componentDidUpdate() {
		// 获取表格

		this.renderTable()

	}
	renderTable() {
		console.log('renderTable')
		// 在下一帧重绘表格
		window.requestAnimationFrame(() => {
			// 获取表格主内容第一行的td
			let tableTd = document.querySelectorAll('.order-table .ant-table-body .ant-table-fixed tr')[0].querySelectorAll('td')
			// 获取表头
			let tableTh = document.querySelectorAll('.order-table .ant-table-header th')
			// 获取左侧固定列
			let fixedLeftTh = document.querySelector('.order-table .ant-table-fixed-left th')
			let fixedLeftTd = document.querySelector('.order-table .ant-table-fixed-left td')
			// 遍历表第一行，比对每个元素宽度，采用最大值设定宽度
			tableTd.forEach((td, index) => {
				let tdWidth = td.offsetWidth
				let thisTh = tableTh[index]
				console.log('thisTh.offsetWidth', td.offsetWidth, thisTh.offsetWidth)
				if (td.offsetWidth > thisTh.offsetWidth) {
					// 表内容的宽度大于等于表头的宽度，给表头加宽
					thisTh.style.minWidth = tdWidth + 'px'
					if (index === 0) {
						console.log("tdWidth", tdWidth)
						fixedLeftTh.style.minWidth = tdWidth + 'px'
						fixedLeftTd.style.minWidth = tdWidth + 'px'
					}
				} else {
					// 给表内容加宽
					td.style.minWidth = thisTh.offsetWidth + 'px'
					if (index === 0) {
						fixedLeftTh.style.minWidth = thisTh.offsetWidth + 'px'
						fixedLeftTd.style.minWidth = thisTh.offsetWidth + 'px'
					}
				}
			})
			if (this && this.setTableHeight) {
				this.setTableHeight()
			}

		})
	}
	onCell(val) {
		console.log(val)
	}
	state = {
		data: []
	}
	setTableHeight() {
		let fatherHeight = document.querySelector('.order-table').parentElement.offsetHeight
		// 给左侧固定列加宽
		document.querySelector('.order-table .ant-table-body-inner').style.maxHeight = (fatherHeight - 131) + 'px'
	}
	render() {

		return (
			<Table onChange={this.renderTable} className="order-table" columns={columns} rowKey={(record, index) => index} dataSource={this.state.data} scroll={{ x: true, y: 306 }} />
		)
	}
}
export default Order