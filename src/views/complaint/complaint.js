import React from 'react'

import { Tabs, Table } from 'antd'

const { TabPane } = Tabs

const columns = [{
		title: '姓名',
		dataIndex: 'name',
		key: 'name',
	},
	{
		title: '年龄',
		dataIndex: 'age',
		key: 'age',
	},
	{
		title: '住址',
		dataIndex: 'address',
		key: 'address',
	}
]
class Complaint extends React.Component {
	state = {
		dataSource: [{
				key: '1',
				name: '啦啦啦',
				age: 32,
				address: '西湖区湖底公园1号',
			},
			{
				key: '2',
				name: '李俊基',
				age: 42,
				address: '西湖区湖底公园1号',
			}
		],
		key: '0'
	}
	componentDidMount() {
		console.log('componentDidMount')
		console.log(this.props.tabs)
		// this.matchKey()
	}
	matchKey(tabs) {
		console.log('matchKey')
		console.log(tabs)
		if (tabs.length) {
			for (let i = 0; i < tabs.length; i++) {
				if (tabs[i].tab === '360') {
					console.log(i, tabs[i].tab)
					this.setState({
						key: i + ''
					})
					break
				}
			}
		}

	}
	// 当props更新是，旧的props还在this里，新的会作为参数传入
	componentWillReceiveProps(newProps) {
		console.log('componentWillReceiveProps')
		console.log(this.props.tabs)
		// console.log(newProps)
		this.matchKey(newProps.tabs)
		// return true
	}
	callback(key) {
		console.log(key)
		this.setState({
			key
		})
	}
	render() {
		return (
			<Tabs defaultActiveKey="1" activeKey={this.state.key} onChange={(key => {this.callback(key)})}>
				{this.props.tabs.map((item, index) => {
					return <TabPane tab={item.tab} key={`${index}`}>
						<Table dataSource = {this.state.dataSource} scroll = {{ y: 300 }} columns = { columns }/>
					</TabPane>
				})}
			</Tabs>
		)
	}
}
export default Complaint