import React, { useState, createContext } from "react";
import { Button } from "antd";

import MyForm from "./MyForm.jsx";
import MyPage from "./MyPage.jsx";
import MyCount from "./MyCount";
console.log(useState)
/** 导出Context，在需要用的页面再导入，可以实现跨页面，，让跨组件调用方法也变得简单了 */
export const ThemeContext = createContext({});
export default function Index() {
  // 只要此页面有任务state变动，都会重新执行, 组件内也会执行（但不会重新渲染）
  console.log('init------------------------')
  const [btnText, setBtnText] = useState("hello, btn!");
  const [btnType, setBtnType] = useState("primary");
  const [count, setCount] = useState(0);
  const [iconType, setIconType] = useState('plus')
  function btnClicked(val:string) {
    console.log('-----', val)
    setBtnType("danger");
    setBtnText("wa, clicked!");
    // setIconType('plus-circle')
  }
  let formProps = {
    data: {
      name: "lili",
      age: 12,
    },
    count,
    dataChange(values: object) {
      // 还是原始的，并不会被改变
      console.log('formProps.data----dataChange', values)
    },
    btnClicked(value: number) {
      console.log('btnClicked----btnClicked', value)
      // 只要此页面有任务state变动，都会重新执行
      setCount(value)
    }
  };
  // setTimeout(() => {
  //   console.log("set count timer");
  //   setCount(10);
  //   console.log("set count timer stop----------", count);
  // }, 2000);
  return (
    <ThemeContext.Provider
      value={{
        iconType,
        setIconType
      }}
    >
      {/* 按钮 */}
      <MyForm btnType={btnType} btnText={btnText} clicked={btnClicked}></MyForm>
	  {/* 表单 */}
      <MyPage {...formProps}> </MyPage>
	  {/* 点击计数器 */}
    按钮
      <MyCount btnType="danger" {...formProps}></MyCount>
    </ThemeContext.Provider>
  );
}
