import React, { useState, useContext } from 'react'
import { Button, Icon } from 'antd'
/** 导入定义过的Context，在index要导出，让跨组件调用方法也变得简单了	 */
import {ThemeContext} from './index'
export default function Bth(props) {
	console.log('myform-----this', props)
	// undefined {btnType: "primary"}
	let {iconType, setIconType} = useContext(ThemeContext)
	// console.log('dataSource----', iconType, setIconType)
	const [btnType, setBtnType] = useState('primary')
	function btnClick() {
		// setBtnType('danger')
		// props.btnType = 'danger'
		// 报错 Cannot assign to read only property 'btnType' of object
		// setBtnText('wa, clicked!')
		setIconType('plus-circle')
		props.clicked('wa, clicked!')
	}
	return <Button type={props.btnType} onClick={btnClick}><Icon type={iconType} />{props.btnText}</Button> 
}