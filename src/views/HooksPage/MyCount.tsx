import React, { useState, useEffect } from 'react'
import { Button } from 'antd'
import {useTitle, useDebounce} from './MyHooks'
// console.log(useTitle)
/**
useEffect 就是一个 Effect Hook，给函数组件增加了操作副作用的能力。
它跟 class 组件中的 componentDidMount、componentDidUpdate 和 componentWillUnmount 具有相同的用途，
只不过被合并成了一个 API。
*/
export default function Bth(props: any) {
	console.log('this my count---------------begin', props.count)
	/** 要用新的变量| 1 */
	const [count, setCount] = useState(props.count)
	// useEffect(() => {
		// console.log('数据又变动，需要更新世图')
		// document.title = `${count}次${props.count} - 你点击了`
	// })
	useTitle(`点击${count}-${props.count}次`)
	useDebounce(()=>{
		// setCount(count)
		console.log(count)
	}, [count])
	function btnClick() {
		// 会报错，不能直接改变+1
		// count = count + 1
		// console.log(count)
		/** 要用新的变量 */ 
		props.btnClicked(props.count+1)
		setCount(count + 1)
	}
	return <>
		<Button type={props.btnType} onClick={btnClick}>你点击了{count}次{props.count}</Button> 
	</>
}