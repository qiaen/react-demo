import { useEffect, useRef } from 'react'

export const useTitle = (title: string) => {
    useEffect(() => {
        document.title = title ? `${title} - REACT` : 'REACT'
    })
}
export const useDebounce = (fn:Function, deps:any[] = [], time: number = 1000) => {
    /** 为什么要用useRef */ 
    let timer:any = useRef()
    useEffect(() => {
        console.log('timer', timer)
        if(timer.current) {clearTimeout(timer.current)}
        timer.current = setTimeout(()=>{
            fn()
        }, time)
    }, deps)
    return
}