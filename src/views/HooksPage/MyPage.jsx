import React, { useState } from 'react'
import { Button, Form, Input, Select } from 'antd'
const { Option } = Select
const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 }
}
const tailLayout = {
    wrapperCol: { offset: 8, span: 16 },
}
const Page = props => {
	console.log(this, props)
	const form = props.form
	const { getFieldDecorator } = form
	const data = props.data || {}
	function submit() {
		props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                props.dataChange(values)
            }
        })
		// props.dataChange()
	}
	return <Form {...layout} name="control-ref">
	    <Form.Item name="name" label="姓名" rules={[{ required: true }]}>
	        {getFieldDecorator('name', { initialValue: data.name,rules: [{ required: true, message: '必填' }] })(
	            <Input placeholder="姓名"/>
	        )}
	    </Form.Item>
	    <Form.Item name="age" label="年龄" rules={[{ required: true }]}>
	        {getFieldDecorator('age', { initialValue: data.age,rules: [{ required: true, message: '必填' }] })(
	            <Select placeholder="选择年龄" allowClear>
	                <Option value={10}>10</Option>
	                <Option value={20}>20</Option>
	                <Option value={30}>30</Option>
	            </Select>
	        )}
	    </Form.Item>
	    <Form.Item {...tailLayout}>
	        <Button onClick={submit} type="primary">Submit</Button>
	    </Form.Item>
	</Form>
}
export default Form.create()(Page)