import React from 'react'

// 顺序要放上面！！！
import RouterView from './router.js'

import { Layout, Menu, Icon } from 'antd';

const { Header, Sider, Content } = Layout;

const menus = [
	{ title: '工单', icon: 'user', path: '/order' },
	{ title: '查询条件', icon: 'video-camera', path: '/search' },
	{ title: '投诉', icon: 'upload', path: '/complaint' },
	{ title: '测试postMessage', icon: 'upload', path: '/post-message' },
	{ title: 'Hooks', icon: 'upload', path: '/hooks' }
]
class App extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			collapsed: false,
		}
		console.log(props)
	}
	toggle = () => {
		this.setState({
			collapsed: !this.state.collapsed
		})
	}
	// 控制菜单折叠
	collapsed = () => {
		return this.state.collapsed ? 'menu-unfold' : 'menu-fold'
	}
	// 点击菜单
	onTap(item) {
		window.location.href = item.path
	}
	render() {
		return (
			<Layout>
				<Sider trigger={null} collapsible collapsed={this.state.collapsed}>
					<div className="logo" />

					<Menu theme="dark" mode="inline">
						{menus.map((item, index) => {
								return (
									<Menu.Item onClick={this.onTap.bind(this, item)} key={index}>
										<Icon type={item.icon} />
										<span>{item.title}</span>
									</Menu.Item>
								)
							})}
					</Menu>
				</Sider>

				<Layout>
					<Header style={{ background: '#fff', padding: '0 15px' }}>
						<Icon className="trigger" type={this.collapsed() } onClick={this.toggle}/>
					</Header>

					<Content style={{ margin: '15px', padding: 15, background: '#fff', minHeight: 280, }}>
						{/*路由*/}
						<RouterView></RouterView>
					</Content>
				</Layout>
			</Layout>
		)
	}
}

export default App