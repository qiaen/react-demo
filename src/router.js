import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Order from './views/orders/order.js'

import Complaint from './views/complaint/complaint.js'

import Search from './views/search/index.jsx'

import Tabs from './views/tabs/tabs.js'

import PostMessage from './views/PostMessage/index.jsx'

import HooksPage from './views/HooksPage/index.tsx'

export default class App extends Component {
	state = {
		tabs: []
	}
	render() {
		return (
			<Router basename="/">
				<Switch>
					<Route exact path='/search' component={Search} />
					<Route exact path='/order' component={Order} />
					<Route exact path='/complaint' component={Complaint} />
					<Route exact path='/post-message' component={PostMessage} />
					<Route exact tabs={this.state.tabs} path='/tabs' component={Tabs} />
					<Route exact path='/hooks' component={HooksPage} />
				</Switch>
			</Router>
		)
	}
}