import { combineReducers } from 'redux'

let initState = {
	subId: '',
	type: 'CAT'
}
const a = (state = initState, action) => {
	switch (action.type) {
		case 'ADD':
			return action.payload
		default:
			return state
	}
}
export default combineReducers({ a })