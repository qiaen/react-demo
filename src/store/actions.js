let nextTodoId = 0

export const addTodo = content => ({
	type: 'ADD',
	payload: {
		id: ++nextTodoId,
		content
	}
})